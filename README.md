# Game of life

An implementation of Conway's game of life in Netlogo.

Author : [Cyprien Borée](https://boreec.fr)

![](http://boreec.fr/wp-content/uploads/2020/11/GameOfLife.gif)